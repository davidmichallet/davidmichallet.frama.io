
# OPENSSH

> Porte d'entré d'un serveur
> SSH = Secure Shell  
> Open source  
> 1995  
> Fonctionnement client > serveur
__

:warning: Mise en garde de sécurité, avant de "copier & coller" des commandes GNU/Linux recoupez vos sources et surtout vérifier qu'elles soient sont à jour, c'est un domaine ou les choses bougent !
# Objectifs :
- Lutter contre les attaques des robots (bots).
- Sécurité des communications entre 2 machines
- Lutter contre les interceptions et l'usurpation



# I. Côté Client SSH <img src="https://openclipart.org/image/400px/171433" alt="drawing" width="20"/>
 :information_source: Pour la suite du tutoriel, le nom du client utilisé est **client** et celui du serveur : **serveur**. Facile !


## 1. Installation du client :

 :information_source: Dans une configuration de type station de travail (bureau), où l'on peut être à la fois **"client"** pour se connecter à des serveurs (web, vpn, bdd...)
mais aussi
**"serveur"**, dans le cas où l'on souhaite accèder depuis notre laptop (télétravail) à des ressources se trouvant sur notre station de travail (bureau).

Si on souhaite être à la fois client et serveur alors il faut lancer la commande.
`sudo apt install openssh`
         
## 2. Générer clefs publique :key2: /privé :key:



>  :information_source: Votre paire de clés publique et privée n'a pas besoin d'être regénéré sauf :
> - si vous estimez que votre clé privée a été corrompue ou volée
> - si l'algorithme que vous avez choisi a été hacké (ça n'arrive pas souvent)

```shell
ssh-keygen -o -a 100 -t ed25519 -C "DM-CEN38"
```

```text
Options :
> -o : Sauvegarde votre clé dans le nouveau format openssh au lieu de l’ancien format PEM.
> -a : C’est le nombre de tour de clé
> -t : type d'algorithme (DSA, RSA, ECDSA, ED25519)
> -C : commentaire pour identifier la clé
- Passphrase pour dévérouiller la clé privée.
```
A noter qu'il est important de mettre une passphrase correcte. Car sans passphrase (ou passphrase trop faible), n'importe qui ayant accès à votre clé privée (vol de PC, récupération des fichiers via une attaque à distance ou physique...) peut accéder sans autre restriction à vos serveurs.
  	
```shell
client@GeoDevOps:~$ ssh-keygen -t ed25519 -C "DM-CEN38"
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/test/.ssh/id_ed25519): 
Created directory '/home/test/.ssh'.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/test/.ssh/id_ed25519
Your public key has been saved in /home/test/.ssh/id_ed25519.pub
The key fingerprint is:
SHA256:vcoN19U6eHW2ih0E0oWftBurYzampn5fS10BUn21axE DM-CEN38
The key's randomart image is:
+--[ED25519 256]--+
|           .o+.Eo|
|          ..o...+|
|         . oo o+.|
|         .. .= .+|
|        S .  .=o*|
|           o.=o++|
|        . o +++o |
|       . B B=.+. |
|      .o*.Boo+   |
+----[SHA256]-----+
```
 

## 3. Transfert notre clef "**publique**" vers le serveur

>  :information_source: On envoie notre clé publique sur l'ensemble de nos serveurs et avec une même passphrase on peut accéder à l'ensemble de nos serveurs ;-) 

> :warning: L'authentification par la paire de clé est plus sécurisé que par mot de passe, on vous recommande d'utiliser cette méthode.

`ssh-copy-id -i ~/.ssh/id_ed25519.pub vagrant@192.168.60.9`

```shell
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: ".ssh/id_ed25519.pub"
The authenticity of host '192.168.60.9 (192.168.60.9)' can't be established.
ECDSA key fingerprint is SHA256:gyZp4EG///8Jguc0942MQ9RdUQUSifAWxd1qrhbWHGs.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes 
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
serveur@192.168.60.9's password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'serveur@192.168.60.9'"
and check to make sure that only the key(s) you wanted were added.
```
- Lister les fichiers .ssh du **client**

 :information_source: A titre d'information, on voit les restrictions d'accès sur les différentes clés :

```shell
client@GeoDevOps:~$ ls -lah ~/.ssh
total 20K
drwx------ 2 client client 4,0K nov.  27 10:37 .
drwxr-xr-x 5 client client 4,0K nov.  27 10:31 ..
-rw------- 1 client client  444 nov.  27 10:31 id_ed25519
-rw-r--r-- 1 client client   90 nov.  27 10:31 id_ed25519.pub
-rw-r--r-- 1 client client  222 nov.  27 10:37 known_hosts
```
> Known_hosts : fichier où est stocké les empreintes (fingerprint) des serveurs sur lesquels vous vous êtes déjà connecté.

- On teste la connexion avec la clé privée et on doit nous demander notre passphrase.
```shell
 ssh serveur@192.168.60.9
``` 

>  :information_source: Pour Windows : Il faut utiliser l'utilitaire [puttygen.exe](https://the.earth.li/~sgtatham/putty/latest/w64/puttygen.exe) pour générer votre paire de clés. A noter que dans les dernières versions de PowerShell, il est possible d'utiliser les commandes énnoncés plus haut et le fonctionnement sous Windows10 est similaire au fonctionnement sous Linux. Pour les autres version de Windows, il est également possible d'utiliser [Cmder](https://cmder.net/) qui est un terminal avec les commandes de base Linux.  

## 4. Mémoriser notre passphrase pour notre session

>  :information_source: Utilisation de ssh-agent permet de mémoriser la passphrase durant la session de la console.
Si vous fermez votre console, l'agent-ssh aura oublié votre passphrase.

- Ajouter la clé à l'agent SSH et mémoriser la passphrase durant la session de la console.
`ssh-add`

```shell
client@GeoDevOps ~ % ssh-add
Enter passphrase for /home/client/.ssh/id_ed25519: 
Identity added: /home/client/.ssh/id_ed25519 (DM-CEN38)
```

- si rien ne s'affiche, on lance un agent-ssh
`
eval `ssh-agent`
`
- Teste de connexion **avec** l'agent-ssh ;-)
`ssh server@192.168.60.2`

> :information_source: Utilisation de ssh-agent pour mémoriser la/les passphrase pendant 36000 secondes  
 `ssh-agent -t 36000`

> :information_source: Outils tiers pour mémoriser la passphrase :

- **Linux**
  - [keychain](https://www.cyberciti.biz/faq/ubuntu-debian-linux-server-install-keychain-apt-get-command/)
  - remmina
	
- **Windows**
  * [Pageant.exe](https://the.earth.li/~sgtatham/putty/latest/w64/pageant.exe)

## 5. Facultatif : Création d'un fichier de configuration

- Création d'un fichier de configuration pour simplifier les connexions serveurs.
`vim ~/.ssh/config`


```
client@GeoDevOps ~ % cat ~/.ssh/config 
Host bob # Petit nom qu'on donne à notre serveur
  HostName 192.168.60.9 # Adresse IP du serveur
  User serveur # utilisateur du serveur auquel on veut avoir accès
  Port 22 # Le port d'écoute du serveur (par défault c'est le 22)
  Protocol 2 # On spécifie qu'on veut utiliser le protocole 2 (plus sécurisé)

Host alice
  HostName 192.168.38.9
  User mich
  Port 3822
  Protocol 2
```
- Teste de la connexion
`ssh bob`
			
# II. SERVEUR <img src="https://openclipart.org/image/800px/171422" alt="drawing" width="30"/>


:warning: Attention au nom d'utilisateur trop classique (*admin, ubuntu, apache, user, postgres ...* )

## 1. Installation du serveur

`sudo apt install openssh-server`

- Vérification qu'on utilise une version > 6.5 pour la prise en charge de l'algorithme ed25519
`ssh -V`

>  :information_source: Lister les fichiers du **serveur**

```
serveur@GeoDevOps:/# ls -la /etc/ssh/
├── moduli
├── ssh_config
├── sshd_config
├── ssh_host_ecdsa_key
├── ssh_host_ecdsa_key.pub
├── ssh_host_ed25519_key
├── ssh_host_ed25519_key.pub
├── ssh_host_rsa_key
└── ssh_host_rsa_key.pub
```

## 2. Modification de la configuration du serveur

>  :information_source: On va modififier le comportement par défault du serveur pour ajouter un peu plus de sécurité.

- Dans un premier temps, on sauvegarder le fichier /etc/ssh/sshd_config
`sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.backup`

- **Modification des options de /etc/ssh/sshd_config**

 `sudo vim /etc/ssh/sshd_config`
 
 >  :information_source: **Minimum :**
- {- Interdire de se connecter en tant que root.-}
- {- Accepter l’authentification par clé (paramètre par défault).-}
- {- Forcer l'utilisation du protocole SSH 2 (man sshd)-}

```
#	$OpenBSD: sshd_config,v 2.101 2017/03/14 07:19:07 djm Exp $
PermitRootLogin no # Permission à se connecter en tant que root
PubkeyAuthentication yes
PermitEmptyPasswords no
ChallengeResponseAuthentication no
# IMPORTANT !
Protocol 2 # Forcer l'utilisation du Protocole 2
```

**Pour plus de sécurité :**

- {+ Changer le port d'écoute de SSH.+}
- {+ Identifier les utilisateurs pouvant se connecter+}
- {+ Interdire la connexion par mot de passe+}

**Visuel :**

- {+ Affichage d'une bannière à la connexion +}
      
```
#	$OpenBSD: sshd_config,v 2.101 2017/03/14 07:19:07 djm Exp $

Port 3822 # Mon nouveau port de connexion

#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
#HostKey /etc/ssh/ssh_host_ed25519_key

# Authentication:

#LoginGraceTime 2m
PermitRootLogin no # Permission à se connecter en tant que root
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10
PubkeyAuthentication yes

# To disable tunneled clear text passwords, change to no here!
PasswordAuthentication no 
PermitEmptyPasswords no

ChallengeResponseAuthentication no
X11Forwarding no # Transfert de l'affichage graphique x11
PrintMotd no

# no default banner path
Banner /etc/ssh/banner # Inscription d'une bannière à la connexion au serveur (juste visuel pour identifier plus facilement)
# Allow client to pass locale environment variables
AcceptEnv LANG LC_*

# override default of no subsystems
Subsystem	sftp	/usr/lib/openssh/sftp-server

# IMPORTANT !
Protocol 2
# EN PLUS 
AllowUsers serveur vagrant # Restreinder l'accès à certains utilisateur
```

> :warning: Après avoir redémarrer le service sshd, garder la session ouverte et **ouvrir un second terminal** pour tester que tout fonctionne, si ce n'est pas le cas, vous aurez toujours accès au serveur depuis votre première console !

`sudo service sshd restart`

- Visualiser les processus sshd qui tournent : 
`pstree | grep ssh`
> On voit que sshd a été dupliqué et que le premier processus sshd fonctionne avec l'ancienne config(premier terminal) et que le deuxième processus tourne avec les nouveaux paramètres.

Pour exemple, mon fichier /etc/ssh/sshd_config


- On teste la connexion
`ssh server@192.168.60.9 -p 3822`
> -p : Par défault, le port d"écoute de ssh est 22
> si on utilise un port différent "-p numero_du_port" 

# III. Pour aller plus loin :rocket:

## Possibilités :
- Rebonds : accéder à une machine (linux) sur un réseau local depuis l'extérieur.
- sshpass
- sshfs : montage réseau via tunnel ssh
- GIT : connection automatique
- ProxySocks : se connecter à son réseau local (comme VPN)
- Utilisation 2FA (Two-Factor authentication)
- Mise en place d'outils type **[Fail2ban](https://github.com/fail2ban/fail2ban)** ou **[crowdsec](https://github.com/crowdsecurity/crowdsec/releases/tag/v1.0.0)** : permet de Bannir une ip pendant au bout x tentative infructeuse.
- Keychain :
 

## a/ Créer un tunnel pour rediriger le port d'une machine distante vers notre machine locale.
- Exemple : je souhaite accéder à ma base Postgresql (distante) sur mon poste local.

> :information_source: Création d'un tunnel chiffré entre ma machine local vers mon serveur distant postgresql entre les ports 5432 (serveur postgres) et mon poste local avec le port 1234.

`ssh -L 1234:localhost:5432 my_user@99.99.99.01 -p22 -f -N`


Pour fermer la connexion

```
ps aux | grep ssh
```

## b/ Accéder à une machine se trouvant sur un réseau local.
- Exemple : je souhaite accéder à mon PC du Bureau depuis mon lieu de télétravail, pour lancer des traitements, accéder à des documents...


1. Depuis son poste de travail au bureau (sur son réseau locak et non accessible diretement depuis internet) sur lequel on souhaite avoir acces depuis l'extérieur.

	 - création d'un tunnel vers un server accessible au web:
	 
`ssh user@99.99.99.99 -R 10000:localhost:22`

	- Sur le poste distant (ex: chez moi) se connecter au server web
	
`ssh user@99.99.99.99 -p 22`

	- Une fois connecté au serveur web via ssh, établir la connexion sur le poste du reseau local
`ssh localhost -p 10000`


# IV. Méthodes de connexion en fonction de l'OS :

### Windows :
- [WSL2](https://www.it-connect.fr/installer-wsl-2-sur-windows-10/)
- [Connexion Putty](https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux/41773-la-connexion-securisee-a-distance-avec-ssh#/id/r-41601)
- [Installer le client SSH de Windows 10](https://www.it-connect.fr/comment-utiliser-le-client-ssh-natif-de-windows-10/)

### Linux

- Terminal
- [Remmina (GUI)](https://remmina.org/)

### Mac
- iterm

# Sources :

[Openclassrooms: La connexion sécurisée à distance avec SSH](https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux/41773-la-connexion-securisee-a-distance-avec-ssh)

# LIENS :

- [Comprendre le clefs ssh](https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux/41773-la-connexion-securisee-a-distance-avec-ssh)


- [Choisir une phrase de passe](https://guide.boum.org/tomes/1_hors_connexions/3_outils/01_choisir_une_phrase_de_passe/)

- [Recommandation de Mozilla pour OpenSSH](https://infosec.mozilla.org/guidelines/openssh)
