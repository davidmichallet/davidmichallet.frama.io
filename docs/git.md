# Git

Git est un outil permettant de versionné son code et facilite le travail en équipe.
Git fonctionne sur le principe de client/server.

## Resouces
- [learngitbranching](https://learngitbranching.js.org/?locale=fr_FR)
- [text](https://marklodato.github.io/visual-git-guide/index-fr.html)
- [text](https://www.atlassian.com/fr/git/tutorials/setting-up-a-repository)
- [bases de git](https://blog.jetpulp.fr/bases-de-git-debutant/)
- [openclassrooms](https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git)
- [Mémo des commandes git](https://gkemayo.developpez.com/tutoriels/git/memo-commandes-git/)

## Installation sous dérivés de DEBIAN

`sudo apt install git`

### Configurartion global de git

```shell
git config --global user.name "Prenom Nom" # Nom d'utilisateur
git config --global user.email email@domaine.extension # - E-mail
git config --global core.editor nano # définir l’éditeur de texte
git config --global --list # affiche les paramètres enregistrés
```


## Créer un dépôt local

`git init`



- Permet d'ignorer des dossiers, fichiers à versionner

exemple de syntaxe :

```ini
.gitignore
id_rsa
var/
logs/*
!logs/config.json
*.jpeg
```
Création des fichiers spécifique à la machine gitignore globale
`git config --global core.excludesfile ~/.gitignore`

## Bien débuter avec git

après avoir initialiser le dépot avec `git init` on peut commencer à travailler :
On créer nos fichiers avec `touch monfichier`
On les ajoute au 'stage' avec 

git add monfichier ou git add .

on commit :
git commit

git commit -m "mes modifications"


## Création d'un raccourci pour hist
```shell
git hist
git config --global alias.hist "log --all --graph --decorate --oneline"
```


## Quel WorkFlow :
Gitflow ?

## Branches

```shell
git branch -a # affiche toutes les branches
git branch my-feature # créer une nouvelle branche
git checkout my-feature  # changer de branche
git checkout -b my-feature # Création d’une branche et basculer sur cette branche
git checkout -d my-feature # Suppression de la branche
```
## Le fichier README.md

Permet d'expliquer le projet en décrivant son contenu avec la syntaxe ***markdown.md***

## .Gitignore
