# QGIS & Ressources cartographique

Afin de simplifier l'accès aux différentes sources d'information (vecteurs, rasters et bases de données), veuillez suivre les indications vous permettant de paramètrer QGIS !

![](img/tutoriel_qgis_flux.webm) 

[//]: <> (Commentaire : La syntaxe au dessus ne s'affiche pas sur MKDOCS, juste pour afficher dans l'éditeur de code de gitlab) 

- Suivez la vidéo pour pouvoir importer les différents flux de données (WMS, WFS, WYZ, Postgresql) dans votre SIG préféré !

<div class="myvideo">
   <video  style="display:block; width:100%; height:auto;" controls loop="loop">
        <source src="https://framagit.org/davidmichallet/davidmichallet.frama.io/-/raw/master/docs/qgis/img/tutoriel_qgis_flux.webm"  type="video/webm"  />
   </video>
</div>

## Accéder aux bases de données Interne :

Télécharger le fichier de paramétrage de Postgis : [Postgresql.xml](https://framagit.org/pole-si-cen38/resources/-/blob/main/gis/liens_XYZ.xml) et suivez la vidéo en choisissant le logo ![postgis](img/postgis.png).


## Flux WFS : données vectorielles

| Structure | Nom                                                                                                                       | Métadonnées                                                                                                                                                                       |
|-----------|---------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| CEN 38    | Zones Humides                                                                                                             |                                                                                                                                                                                   |
| CEN 38    | Pelouses sèches                                                                                                           |                                                                                                                                                                                   |
| SEREMA    | Observations de collisions faune sur le réseau routier :  tronçons accidentogènes en Auvergne-Rhône-Alpes en 2018 et 2019 | [**Fiche Métadonnées**](https://www.cdata.cerema.fr/geonetwork/srv/fre/catalog.search;jsessionid=3EBDE215C983CBF5EA2B23BDE5FA398D#/metadata/ef82e1a5-c4c3-4f96-bcc7-2afde7df5f82) |

- CRAIG

| Serveurs WFS                         | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | Accès                           |
|--------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------|
| https://wfs.craig.fr/ign             | Ce service wfs permet de "consulter, interroger, télécharger" la BDCARTO et la BDFORET de l'IGN sur Auvergne - Rhône - Alpes. [Consulter la métadonnée de service](https://ids.craig.fr/geocat/srv/fre/catalog.search#/metadata/6ccdb1a56683ea1d64fe38e4ffa0ffb0bbe8a242) Nota : L'accès à ce flux WFS se fait via une authentification utilisant le même couple nom d'utilisateur/mot de passe requis pour se connecter sur le site du CRAIG                                                                                                                                                     | Membres avec une offre complète |
| https://wfs.craig.fr/rge             | Ce service wfs permet de "consulter, interroger, télécharger" la BDParcellaire Vecteur, la BDTopo et la BD Point Adresse de l'IGN sur Auvergne - Rhône - Alpes. [Consulter la métadonnée de service](https://ids.craig.fr/geocat/srv/fre/catalog.search#/metadata/24cffc35380d4200dd40d746f1bc780385fa823b) Nota : L'accès à ce flux WFS se fait via une authentification utilisant le même couple nom d'utilisateur/mot de passe requis pour se connecter sur le site du CRAIG                                                                                                                   | Membres avec une offre complète |
| http://ids.craig.fr/wxs/public/wfs   | Ce service wfs permet de "consulter, interroger, télécharger" l'ensemble des données "thématiques" déposé par les utilisateurs de la plateforme. Ex : données du réseau routier départemental des Conseils généraux   [Consulter la métadonnée de service](https://ids.craig.fr/geocat/srv/fre/catalog.search#/metadata/dc5deea2c0113bb91cf0aee48572d7434a232e57)                                                                                                                                                                                                                                 | Public                          |
| https://ids.craig.fr/wxs/membres/wfs | Ce service wms permet de "consulter, interroger, télécharger" l'ensemble des données "thématiques" déposé par les utilisateurs de la plateforme dont l'accès est restreint à cause d'une licence ou encadré par une convention. Ex : réseau électrique de 63 kV à 400 kV de RTE   [Consulter la métadonnée de service](https://ids.craig.fr/geocat/srv/fre/catalog.search#/metadata/0851e449a302fa3fbda96fa2d18d241d696c2a16) Nota : L'accès à ce flux WFS se fait via une authentification utilisant le même couple nom d'utilisateur/mot de passe requis pour se connecter sur le site du CRAIG | Membres                         |

Télécharger le fichier
[WFS-CRAIG](https://www.craig.fr/sites/www.craig.fr/files/fichiers/Connexion-CRAIG_wfs_QGIS_201905.zip)


- CEN 38
#### Inventaire des Zones humides
liens à coler dans QGIS `http://ws.carmencarto.fr/WFS/82/INV_ZH38_01_17_11?`

#### Inventaire des pelouses sèches


- SEREMA

#### Observations de collisions faune sur le réseau routier : tronçons accidentogènes en Auvergne-Rhône-Alpes en 2018 et 2019
[**Fiche Métadonnées** : ](https://www.cdata.cerema.fr/geonetwork/srv/fre/catalog.search;jsessionid=3EBDE215C983CBF5EA2B23BDE5FA398D#/metadata/ef82e1a5-c4c3-4f96-bcc7-2afde7df5f82)

liens à coler dans QGIS `https://datacarto.cdata.cerema.fr/wfs/fff6bcdd-25d0-4e3b-a9d7-c2c8a3748869`

## Flux WMS : Fonds RASTERS

- CRAIG

| Serveurs WMS                         | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                       | Accès                           |
|--------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------|
| http://wms.craig.fr/ortho            | Ce service wms permet de "co-visualiser" l'ensemble des orthophotographies produites par le CRAIG et ses partenaires sur la région Auvergne. [Consulter la métadonnée de service](https://ids.craig.fr/geocat/srv/fre/catalog.search#/metadata/db254a5bbc04452aa9aa205bec2fe2c097d6a850)                                                                                                                                                                                                                                                                                   | Public                          |
| http://wms.craig.fr/mnt              | Ce service wms permet de "co-visualiser" l'ensemble des Modèles Numériques de Terrain produits par le CRAIG et ses partenaires. Pour un meilleur confort d'utilisation il est conseillé d'utiliser le service tuilé (http://tiles.craig.fr/mnt/service) plutôt que ce service. [Consulter la métadonnée de service](http://ids.craig.fr/geocat/apps/search/?uuid=2f5f62b2f0afc99f7914fae16d661d99aa2775f7)                                                                                                                                                   | Public                          |
| https://wms.craig.fr/ign             | Ce service wms permet de "co-visualiser" les différentes echelles et variantes de SCANS, l'ORTHOHR, la BDFORET et la BDCARTO de l'IGN sur la région Auvergne - Rhône - Alpes. [Consulter la métadonnée de service](http://ids.craig.fr/geocat/apps/search/?uuid=fa4a4488d9551ba005b4c31bae46d3afceca42ce) Nota : L'accès à ce flux WMS se fait via une authentification utilisant le même couple nom d'utilisateur/mot de passe requis pour se connecter sur le site du CRAIG                                                                              | Membres avec une offre complète |
| http://tiles.craig.fr/pci/service    | Ce service wms permet de "co-visualiser" le cadastre vectoriel sur Auvergne - Rhône - Alpes, uniquement sur les zones ou le cadastre est vectorisé. [Consulter la métadonnée de service](https://ids.craig.fr/geocat/srv/fre/catalog.search#/metadata/eb583907463c3d064865a9391338a9f275420320)                                                                                                                                                                                                                                                                            | Public                          |
| https://wms.craig.fr/bdortho         | Ce service wms permet de "co-visualiser" les anciennes BDORTHO de l'IGN millésimes 1999 a 2005 sur Auvergne - Rhône - Alpes. [Consulter la métadonnée de service](https://ids.craig.fr/geocat/srv/fre/catalog.search#/metadata/4b1f0be7e4a874f621e7ab419f1f894bd8245498) Nota : L'accès à ce flux WMS se fait via une authentification utilisant le même couple nom d'utilisateur/mot de passe requis pour se connecter sur le site du CRAIG                                                                                                                               | Membres avec une offre complète |
| https://wms.craig.fr/rge             | Ce service wms permet de "co-visualiser" la BD Ortho, le RGE Alti, BD Parcellaire, la BD Topo et la BD Point Adresse de l'IGN sur Auvergne-Rhône-Alpes. [Consulter la métadonnée de service](https://ids.craig.fr/geocat/srv/fre/catalog.search#/metadata/3989da5f9ef1095ec21b64a2d696430ae9fa776f) Nota : L'accès à ce flux WMS se fait via une authentification utilisant le même couple nom d'utilisateur/mot de passe requis pour se connecter sur le site du CRAIG                                                                                                    | Membres avec une offre complète |
| http://ids.craig.fr/wxs/public/wms   | Ce service wms permet de "co-visualiser" l'ensemble des données "thématiques" déposé par les utilisateurs de la plateforme. Ex : données du réseau routier départemental des Conseils généraux [Consulter la métadonnée de service](https://ids.craig.fr/geocat/srv/fre/catalog.search#/metadata/ec35703aaa56bb023dfde6e50a0ae43bc0377dea)                                                                                                                                                                                                                                 | Public                          |
| https://ids.craig.fr/wxs/membres/wms | Ce service wms permet de "co-visualiser" l'ensemble des données "thématiques" déposé par les utilisateurs de la plateforme dont l'accès est restreint à cause d'une licence ou encadré par une convention. Ex : réseau électrique de 63 kV à 400 kV de RTE [Consulter la métadonnée de service](https://ids.craig.fr/geocat/srv/fre/catalog.search#/metadata/ec35703aaa56bb023dfde6e50a0ae43bc0377dea) Nota : L'accès à ce flux WMS se fait via une authentification utilisant le même couple nom d'utilisateur/mot de passe requis pour se connecter sur le site du CRAIG | Membres                         |

Télécharger le fichier
[WMS-CRAIG](https://www.craig.fr/sites/www.craig.fr/files/fichiers/Connexion-CRAIG_wms_QGIS_201905.zip)

## Flux XYZ (autres fonds raster)

| Structure      	| Nom                    	| Métadonnées 	|
|----------------	|------------------------	|-------------	|
| Carto Positron 	|                        	|             	|
| OSM            	| Open Street Map en N&B 	|             	|
| OSM            	| Open Street Map        	|             	|
| OSM            	| OpenCycleMap           	|             	|
| OSM            	| Opentopomap            	|             	|
| Stamen Terrain 	|                        	|             	|
| ARCGIS         	| ESRI Topo              	|             	|
| GOOGLE         	| Maps                   	|             	|
| GOOGLE         	| Street                 	|             	|
| GOOGLE         	| Satellite              	|             	|

![XYZ](gis/img/XYZ.png "test")
